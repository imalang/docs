---
icon: ":book:"
---
# Resources

## Courses

- [Speech and Language Processing](https://web.stanford.edu/~jurafsky/slp3/) (Dan Jurafsky & James H. Martin - Stanford)
- [LLM Distillation Playbook](https://github.com/predibase/llm_distillation_playbook) (Justin Zhao & Wael Abid - Predibase)
- [Computer Science](https://stanford.edu/~shervine/teaching/) (Shervine & Afshrine Amidi - MIT & Stanford) 

## Blogs

- [Niklas Heidloff - IBM](https://heidloff.net/) (NLP, LLM, Production)

## Bibliography

- [ML Papers of the Week](https://github.com/dair-ai/ML-Papers-of-the-Week?tab=readme-ov-file)
- [LLMpedia](https://llmpedia.streamlit.app/) ([GitHub](https://github.com/masta-g3/llmpedia))