---
order: 1
author:
  - François Ledoyen
---

# SLURM guide

## Inspect running job

You can further inspect a running job by connecting to it with this command :

```sh
srun --jobid=jobid --overlap --pty bash
```

This will open an interactive shell as a job step under an already allocated
job. I.e. you will be able to see how your job is behaving. For distributed
memory jobs you will get a shell at the first node used by your job.

## Debug in Real-time on SLURM

### Problem

Debugging a code by submitting jobs to a supercomputer is an inefficient
process. It goes something like this :

1. Submit job and wait in queue.
1. Check for errors/change code.
1. Repeat endlessly until your code works.

### Solution : interactive jobs (`srun --pty`)

Fortunately, there’s a better way, you can debug in real-time like so:

1. Request a debugging or interactive node and wait in queue :

   ```sh
   srun --partition=<name> --nodes=<nnodes> --gres=gpu:<ngpus> --time=<time> --pty bash -i
   ```

1. This is how it looks once the interactive job starts :

   ```sh
   srun: job 12345 queued and waiting for resources
   srun: job 12345 has been allocated resources
   ```

1. Check for errors/change code continuously until code is fixed or node has
   timed out.

## Sources

- [HPC-UiT Services User Documentation](https://hpc-uit.readthedocs.io/en/latest/jobs/interactive.html#starting-an-interactive-job)
- [Innsbruck University's SLURM Tutorial](https://www.uibk.ac.at/zid/systeme/hpc-systeme/common/tutorials/slurm-tutorial.html)
