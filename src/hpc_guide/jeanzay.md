---
label: Jean Zay
---
# Jean Zay

## `Illegal Instruction` Error on partion `gpu_p5`

### Why ?

This error indicates that you have used modules or code incompatible with the AMD CPUs equipping this partition [`gpu_p5`](http://www.idris.fr/eng/jean-zay/gpu/jean-zay-gpu-exec_partition_slurm-eng.html#les_partitions_available "web:eng:jean-zay:gpu:jean-zay-gpu-exec_partition_slurm-eng"). 

### Solution

So in your slurm script you should, first load `cpuarch/amd` and then load your pytorch module.

---
Source : http://www.idris.fr/eng/faqs/illegal_instruction-eng.html 