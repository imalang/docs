# ImaLang Group Knowledge Sharing Website

- The site is accessible at https://imalang.pages.unicaen.fr/docs/
- It is generated using [Retype](https://retype.com/) which allows to create
  static website with no coding, just markdown.
- It is automatically deployed using
  [Gitlab Pages](https://docs.gitlab.com/ee/user/project/pages/) and updated
  with each push through a CI/CD pipeline.

## To contribute

- Clone the repository :

```bash
git clone git@git.unicaen.fr:imalang/docs.git
```

- Install `Retype` :

```bash
npm install retypeapp --global
```

- Launch the local server to preview changes in real-time:

```bash
retype start
```

- Follow the [retype documentation](https://retype.com/guides/formatting/) to
  edit markdown files.

- Finally, just push the changes and wait for the pipeline to rebuild the site.
